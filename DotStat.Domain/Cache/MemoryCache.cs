﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace DotStat.Domain.Cache
{
    public class MemoryCache : ICache
    {
        private readonly ConcurrentDictionary<string, object> _cache;

        public MemoryCache()
        {
            _cache = new ConcurrentDictionary<string, object>();
        }

        public bool TryGetValue<T>(string key, out T value)
        {
            if (_cache.TryGetValue(key, out var cachedValue))
            {
                value = (T)cachedValue;
                return true;
            }

            value = default;
            return false;
        }

        public void Set<T>(string key, object obj)
        {
            _cache[key] = obj;
        }
    }
}
