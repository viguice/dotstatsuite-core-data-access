﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Domain
{
    public class TransferContent
    {        
        /// <summary>
        /// Observation values, observation level attributes and optionally 
        /// non-observation attributes reported at observation level
        /// </summary>
        public IAsyncEnumerable<ObservationRow> DataObservations { get; set; }

        /// <summary>
        /// Metadata attributes reported in the IObservation
        /// </summary>
        public IAsyncEnumerable<ObservationRow> MetadataObservations { get; set; }

        /// <summary>
        /// Dataset level attributes
        /// </summary>
        public IList<DataSetAttributeRow> DatasetAttributes { get; set; }

        /// <summary>
        /// Reported components in the data source file
        /// </summary>
        public ReportedComponents ReportedComponents { get; set; }

        /// <summary>
        /// Flag to indicate if source was XML file
        /// </summary>
        public bool IsXMLSource { get; set; } = false;
    }

    public class ReportedComponents
    {
        public List<Dimension> Dimensions = new();
        public Dimension TimeDimension;
        public List<Attribute> DatasetAttributes = new();
        public List<Attribute> SeriesAttributesWithNoTimeDim = new();
        /// <summary>
        /// Observation level attributes including series level attributes referencing time dimension
        /// </summary>
        public List<Attribute> ObservationAttributes = new();
        public List<MetadataAttribute> MetadataAttributes = new();
        public bool IsPrimaryMeasureReported = false;
        public bool IsMetadataReported()=> MetadataAttributes.Any();
        public bool IsDataReported() => IsPrimaryMeasureReported || DatasetAttributes.Any() || SeriesAttributesWithNoTimeDim.Any() || ObservationAttributes.Any();
        public CultureInfo CultureInfo;
    }

    public class BatchAction
    {
        public readonly int BatchNumber;
        public readonly StagingRowActionEnum Action;
        public readonly bool IsWildCarded;

        public BatchAction(int batchNumber, StagingRowActionEnum action, bool isWildCarded)
        {
            BatchNumber = batchNumber;
            Action = action;
            IsWildCarded = isWildCarded;
        }
    }

    public class ObservationRow
    {
        public readonly int BatchNumber;
        public readonly IObservation Observation;
        public readonly StagingRowActionEnum Action;
        public readonly bool IsWildCarded;

        public ObservationRow(int batchNumber, StagingRowActionEnum action, IObservation observation, bool isWildCarded)
        {
            BatchNumber = batchNumber;
            Action = action;
            Observation = observation;
            IsWildCarded = isWildCarded;
        }
    }

    public class DataSetAttributeRow
    {
        public readonly int BatchNumber;
        public IList<IKeyValue> Attributes = new List<IKeyValue>();
        public StagingRowActionEnum Action;

        public DataSetAttributeRow(int batchNumber, StagingRowActionEnum action)
        {
            BatchNumber = batchNumber;
            Action = action;
        }
    }

    public enum StagingRowActionEnum
    {
        Skip = 0,
        Merge = 1,
        Replace = 2,
        Delete = 3,
        DeleteAll = 4
    }

    public class IntentionallyMissingValue
    {
        public string Value { get; }

        public static IntentionallyMissingValue Numerical => new("NaN");
        public static IntentionallyMissingValue Textual => new("#N/A");

        private IntentionallyMissingValue(string value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value;
        }
    }
}