﻿using System;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;

namespace DotStat.Domain
{
    public class SdmxVersion : IComparable
    {
        public SdmxVersion()
        {
            Major = 1;
            Minor = 0;
            Patch = null;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return this == (SdmxVersion) obj;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Major;
                hashCode = (hashCode*397) ^ Minor;
                hashCode = (hashCode*397) ^ Patch.GetHashCode();
                return hashCode;
            }
        }

        public readonly int Major;
        public readonly int Minor;
        public readonly int? Patch;

        public SdmxVersion(int major, int minor = 0, int? patch = null)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
        }

        public SdmxVersion(string version) : this()
        {
            if(!Org.Sdmxsource.Util.VersionableUtil.ValidVersion(version))
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongSdmxVersionFormat), version));

            var arr     = version.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            var ver     = new int?[] {null, null, null};

            for (var i = 0; i < Math.Min(arr.Length, 3); i++)
            {
                ver[i] = int.Parse(arr[i]);
            }

            Major   = ver[0] ?? 1;
            Minor   = ver[1] ?? 0;
            Patch   = ver[2];
        }

        public override string ToString()
        {
            return Major.ToString() + '.' + Minor + ( Patch.HasValue ? '.' + Patch.ToString() : null );
        }

        public static int CompareTo(SdmxVersion version1, SdmxVersion version2)
        {
            if (version1 is null && version2 is null)
            {
                return 0;
            }

            if (version1 is null)
            {
                return -1;
            }

            if (version2 is null)
            {
                return 1;
            }


            int compareResult = version1.Major.CompareTo(version2.Major);
            if (compareResult != 0)
            {
                return compareResult;
            }

            compareResult = version1.Minor.CompareTo(version2.Minor);
            if (compareResult != 0)
            {
                return compareResult;
            }

            compareResult = (version1.Patch ?? 0).CompareTo(version2.Patch ?? 0);
            return compareResult;
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            var versionToCompareTo = obj as SdmxVersion;

            if (versionToCompareTo == null)
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotASdmxVersionObject));
            }

            return CompareTo(this, versionToCompareTo);
        }

        public static bool operator <(SdmxVersion version1, SdmxVersion version2) => CompareTo(version1, version2) < 0;

        public static bool operator >(SdmxVersion version1, SdmxVersion version2) => CompareTo(version1, version2) > 0;

        public static bool operator ==(SdmxVersion version1, SdmxVersion version2) => CompareTo(version1, version2) == 0;

        public static bool operator !=(SdmxVersion version1, SdmxVersion version2) => CompareTo(version1, version2) != 0;

        public static bool operator <=(SdmxVersion version1, SdmxVersion version2) => CompareTo(version1, version2) <= 0;

        public static bool operator >=(SdmxVersion version1, SdmxVersion version2) => CompareTo(version1, version2) >= 0;
    }

}
