using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Util;
using DotStat.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using Attribute = DotStat.Domain.Attribute;
using Dsd = DotStat.Domain.Dsd;
using MetadataAttribute = DotStat.Domain.MetadataAttribute;
using PrimaryMeasure = DotStat.Domain.PrimaryMeasure;

namespace DotStat.Db
{
    public static class DbExtensions
    {
        public const string ValueColumn = "[VALUE]";
        public const string TimeDimColumn = "PERIOD_SDMX";
        public const string LAST_UPDATED_COLUMN = "LAST_UPDATED";
        public const string ArtefactTable = "ARTEFACT";
        public const string DimensionSwitchedOff = "~";
        public const string DimensionWildCarded = "*";
        public const int DimensionWildCardedDbValue = -1;
        public const int DimensionSwitchedOffDbValue = 0;
        public const int ColumnPresentDbValue = 1;
        public const string EmbargoTime = "EMBARGO_TIME";

        private const string MaxTextAttributeLengthAnnotation = "MaxTextAttributeLength";

        #region Naming conventions

        public static string SqlColumn(this Dimension dimension, bool externalColumn = false, string prefix = null, string suffix = null)
        {
            return
                externalColumn ? 
                    $"[{prefix}{dimension.Code}{suffix}]" :
                    dimension.Base.TimeDimension ?
                        $"{prefix}{TimeDimColumn}{suffix}" :
                        $"[{prefix}DIM_{dimension.DbId}{suffix}]";
        }

        public static string SqlArtefactTableName(string alias = null)
        {
            return $"{ArtefactTable} {alias}".Trim();
        }

        public static string SqlPeriodStart(bool withType = false, bool withHourSupport = false, bool allowNull = false)
        {
            return SqlPeriod("PERIOD_START", withType, withHourSupport, allowNull);
        }

        public static string SqlPeriodEnd(bool withType = false, bool withHourSupport = false, bool allowNull = false)
        {
            return SqlPeriod("PERIOD_END", withType, withHourSupport, allowNull);
        }

        private static string SqlPeriod(string column, bool withType, bool withHourSupport, bool allowNull = false)
        {
            return column + (withType ? $" {(withHourSupport ? "datetime2(0)" : "date")} {(allowNull?"NULL":"NOT NULL")}" : "");
        }

        public static string SqlColumn(this Attribute attribute, bool externalColumn = false)
        {
            return externalColumn ? $"[{attribute.Code}]" : $"[COMP_{attribute.DbId}]";
        }

        public static string SqlColumn(this MetadataAttribute metaAttribute, bool externalColumn = false)
        {
            return externalColumn ? $"[{metaAttribute.HierarchicalId}]": $"[COMP_{metaAttribute.DbId}]";
        }

        public static string SqlCollate(this PrimaryMeasure primaryMeasure)
        {
            return !primaryMeasure.Base.HasCodedRepresentation() &&
                   GetSqlType(primaryMeasure, primaryMeasure.Dsd.SupportsIntentionallyMissingValues).Contains("varchar")
                ? " COLLATE Latin1_General_CS_AS "
                : string.Empty;
        }

        public static string SqlCollate(this Attribute attribute)
        {
            return !attribute.Base.HasCodedRepresentation() && 
                   GetSqlType(attribute, true, false, null).Contains("varchar")
                ? " COLLATE Latin1_General_CS_AS " 
                : string.Empty;
        }

        public static string SqlCollate(this MetadataAttribute metaAttribute)
        {
            return !metaAttribute.Base.HasCodedRepresentation() &&
                   GetSqlType(metaAttribute, true).Contains("varchar")
                ? " COLLATE Latin1_General_CS_AS "
                : string.Empty;
        }

        public static string SqlStagingTable(this Dsd dsd, string alias = null)
        {
            return SqlStagingTable(dsd.DbId, alias);
        }

        public static string SqlStagingTable(int dsdDbId, string alias = null)
        {
            return $"DSD_{dsdDbId}_STAGING {alias}".Trim();
        }

        public static string SqlFactTable(int dbId, char tableVersion, string alias = null)
        {
            return $"FACT_{dbId}_{tableVersion} {alias}".Trim();
        }

        public static string SqlFactTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return SqlFactTable(dsd.DbId, tableVersion, alias);
        }

        public static string SqlFactHistoryTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return $"FACT_{dsd.DbId}_{tableVersion}_History {alias}".Trim();
        }

        public static string SqlDeletedTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return $"DELETED_{dsd.DbId}_{tableVersion} {alias}".Trim();
        }

        public static string SqlFilterTable(this Dsd dsd, string alias = null)
        {
            return $"FILT_{dsd.DbId} {alias}".Trim();
        }
        

        public static string SqlDimGroupAttrTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return SqlDimGroupAttrTable(dsd.DbId, tableVersion, alias);
        }

        public static string SqlDimGroupAttrTable(int dbId, char tableVersion, string alias = null)
        {
            return $"ATTR_{dbId}_{tableVersion} {alias}".Trim();
        }

        public static string SqlDimGroupAttrHistoryTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return $"ATTR_{dsd.DbId}_{tableVersion}_History {alias}".Trim();
        }

        public static string SqlDsdAttrTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return SqlDsdAttrTable(dsd.DbId, tableVersion, alias);
        }
        public static string SqlDsdAttrTable(int dbId, char tableVersion, string alias = null)
        {
            return $"ATTR_{dbId}_{tableVersion}_DF {alias}".Trim();
        }

        public static string SqlDsdAttrHistoryTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return $"ATTR_{dsd.DbId}_{tableVersion}_DF_History {alias}".Trim();
        }

        public static string SqlMetadataStagingTable(this Dsd dsd, string alias = null)
        {
            return SqlMetadataStagingTable(dsd.DbId, alias);
        }

        public static string SqlMetadataStagingTable(int dbId, string alias = null)
        {
            return $"MSD_{dbId}_STAGING {alias}".Trim();
        }

        public static string SqlMetadataDataStructureTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return SqlMetadataDataStructureTable(dsd.DbId, tableVersion, alias);
        }

        public static string SqlMetadataDataStructureTable(int dbId, char tableVersion, string alias = null)
        {
            return $"META_DSD_{dbId}_{tableVersion} {alias}".Trim();
        }

        public static string SqlMetadataDataFlowTable(this Dataflow dataFlow, char tableVersion, string alias = null)
        {
            return SqlMetadataDataFlowTable(dataFlow.DbId, tableVersion, alias);
        }

        public static string SqlMetadataDataFlowTable(int dbId, char tableVersion, string alias = null)
        {
            return $"META_DF_{dbId}_{tableVersion} {alias}".Trim();
        }

        public static string SqlMetadataDataSetTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return SqlMetadataDataSetTable(dsd.DbId, tableVersion, alias);
        }

        public static string SqlMetadataDataSetTable(int dbId, char tableVersion, string alias = null)
        {
            return $"META_DS_{dbId}_{tableVersion} {alias}".Trim();
        }

        public static string SqlDeletedMetadataTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return SqlDeletedMetadataTable(dsd.DbId, tableVersion, alias);
        }

        public static string SqlDeletedMetadataTable(int dbId, char tableVersion, string alias = null)
        {
            return $"DELETED_META_{dbId}_{tableVersion} {alias}".Trim();
        }

        public static string SqlMetadataAttributeTable(string alias = null)
        {
            return $"METADATA_ATTRIBUTE {alias}".Trim();
        }

        public static string SqlDataDsdViewName(this Dsd dsd, char viewVersion)
        {
            return SqlDataDsdViewName(dsd.DbId, viewVersion);
        }

        public static string SqlDataDsdViewName(int dsdDbId, char viewVersion)
        {
            return $"VI_CurrentDataDsd_{dsdDbId}_{viewVersion}";
        }
        
        public static string SqlDataDataFlowViewName(this Dataflow dataflow, char viewVersion)
        {
            return SqlDataDataFlowViewName(dataflow.DbId, viewVersion);
        }

        public static string SqlDataDataFlowViewName(int dataFlowDsdId, char viewVersion)
        {
            return $"VI_CurrentDataDataFlow_{dataFlowDsdId}_{viewVersion}";
        }

        public static string SqlDataReplaceDataFlowViewName(this Dataflow dataflow, char viewVersion)
        {
            return SqlDataReplaceDataFlowViewName(dataflow.DbId, viewVersion);
        }

        public static string SqlDataReplaceDataFlowViewName(int dataFlowDsdId, char viewVersion)
        {
            return $"VI_CurrentDataReplaceDataFlow_{dataFlowDsdId}_{viewVersion}";
        }

        public static string SqlDataIncludeHistoryDataFlowViewName(this Dataflow dataflow, char viewVersion)
        {
            return SqlDataIncludeHistoryDataFlowViewName(dataflow.DbId, viewVersion);
        }

        public static string SqlDataIncludeHistoryDataFlowViewName(int dataFlowDsdId, char viewVersion)
        {
            return $"VI_DataDataFlow_{dataFlowDsdId}_{viewVersion}_IncludeHistory";
        }

        public static string SqlMetaDataDsdViewName(this Dsd dsd, char viewVersion)
        {
            return SqlMetaDataDsdViewName(dsd.DbId, viewVersion);
        }

        public static string SqlMetaDataDsdViewName(int dsdDbId, char viewVersion)
        {
            return $"VI_MetadataDsd_{dsdDbId}_{viewVersion}";
        }

        public static string SqlMetadataDataFlowViewName(this Dataflow dataFlow, char viewVersion)
        {
            return SqlMetadataDataFlowViewName(dataFlow.DbId, viewVersion);
        }

        public static string SqlMetadataDataFlowViewName(int dataFlowDsdId, char viewVersion)
        {
            return $"VI_MetadataDataFlow_{dataFlowDsdId}_{viewVersion}";
        }

        public static string SqlDeletedMetadataDataFlowViewName(this Dataflow dataFlow, char viewVersion)
        {
            return SqlDeletedMetadataDataFlowViewName(dataFlow.DbId, viewVersion);
        }

        public static string SqlDeletedMetadataDataFlowViewName(int dataFlowDsdId, char viewVersion)
        {
            return $"VI_DeletedMetadataDataFlow_{dataFlowDsdId}_{viewVersion}";
        }

        public static string SqlDeletedMetadataDsdViewName(this Dsd dsd, char viewVersion)
        {
            return SqlDeletedMetadataDsdViewName(dsd.DbId, viewVersion);
        }

        public static string SqlDeletedMetadataDsdViewName(int dsdId, char viewVersion)
        {
            return $"VI_DeletedMetadataDsd_{dsdId}_{viewVersion}";
        }
        public static string GetCodelistTableName(int codelistId)
        {
            return $"CL_{(codelistId == 0 ? "TIME" : codelistId.ToString())}";
        }

        public static string SqlDeletedDataViewName(this Dataflow dataflow, char viewVersion)
        {
            return SqlDeletedDataViewName(dataflow.DbId, viewVersion);
        }

        public static string SqlDeletedDataViewName(int dataFlowDbId, char viewVersion)
        {
            return $"VI_DeletedDataDataFlow_{dataFlowDbId}_{viewVersion}";
        }

        #endregion

        public static string SqlBuildRowIdFormula(this Dsd dsd, bool includeTimeDim = true)
        {
            var str = new StringBuilder();

            foreach (var dim in dsd.Dimensions)
            {
                if (!dim.Base.TimeDimension)
                {
                    str.Append(dim.Base.HasCodedRepresentation()
                                ? $"CONVERT([binary](3), ISNULL([DIM_{dim.DbId}], 0))"
                                : $"CONVERT(binary(16), HASHBYTES('md5', ISNULL([DIM_{dim.DbId}], '')))"
                        )
                        .Append('+');
                }
            }

            if (dsd.TimeDimension != null && includeTimeDim)
            {
                str.Append("CONVERT([binary](3), ISNULL([DIM_TIME], 0))+");
            }

            return str.ToString(0, str.Length - 1);
        }
        
        public static IEnumerable<Dimension> OrderTimeFirst(this IEnumerable<Dimension> dims)
        {
            return dims.OrderBy(x => x.Base.TimeDimension ? 0 : 1);
        }

        public static string ToColumnList(this IEnumerable<Dimension> dimensions, bool withType = false, bool allowNull=false, bool externalColumn = false)
        {
            return string.Join(", ", dimensions.OrderTimeFirst()
                .Select(dim => dim.SqlColumn(externalColumn) + dim.GetSqlType(withType, allowNull)));
        }

        public static string ToColumnList(this IEnumerable<Attribute> attributes, 
            int? maxTextAttributeLength = null, 
            IList<AttributeAttachmentLevel> levelFilter = null,
            bool withType = false, bool applyNotNull = true, bool externalColumn = false)
        {
            return string.Join(",", attributes
                .Where(attr => levelFilter == null || levelFilter.Contains(attr.Base.AttachmentLevel))
                .Select(attr => attr.SqlColumn(externalColumn) + attr.GetSqlType(withType, applyNotNull, maxTextAttributeLength))
            );
        }

        public static string ToColumnListWithIsNull(this IEnumerable<Attribute> attributes, IList<AttributeAttachmentLevel> levelFilter = null)
        {
            return string.Join(" AND ", attributes
                .Where(attr => levelFilter == null || levelFilter.Contains(attr.Base.AttachmentLevel))
                .Select(attr => $"{attr.SqlColumn()} IS NULL")
            );
        }

        public static string ToColumnList(this IEnumerable<MetadataAttribute> metadataAttributes,
            bool withType = false, bool externalColumn = false)
        {
            return string.Join(",", metadataAttributes.Select(attr => attr.SqlColumn(externalColumn) + attr.GetSqlType(withType)));
        }

        public static string ToColumnListWithIsNull(this IEnumerable<MetadataAttribute> metadataAttributes)
        {
            return string.Join(" AND ", metadataAttributes.Select(attr => $"{attr.SqlColumn()} IS NULL"));
        }

        public static SqlDbType GetSqlType(this Attribute attr)
        {
            return attr.Base.HasCodedRepresentation() 
                ? SqlDbType.Int 
                : attr.Code.Equals(EmbargoTime, StringComparison.InvariantCultureIgnoreCase)
                    ? SqlDbType.DateTime2
                    : SqlDbType.NVarChar;
        }

        public static string GetSqlType(this Attribute attr, bool withType, bool applyNotNull, int? maxTextAttributeLength)
        {
            if (!withType)
            {
                return null;
            }

            string sqlType;

            if (attr.Base.HasCodedRepresentation())
            {
                sqlType = "[int]";
            }
            else
            {
                if (attr.Code.Equals(EmbargoTime, StringComparison.InvariantCultureIgnoreCase))
                {
                    sqlType = "[datetime2]";
                }
                else
                {
                    var maxAttributeLength = attr.Dsd.MaxTextAttributeLength ?? attr.Dsd.GetMaxLengthOfTextAttributeFromConfig(maxTextAttributeLength);

                    //A value of less or equal to zero `0` or any value higher than **4000** means that the textual attribute values are practically unlimited
                    sqlType = $"[nvarchar]({(maxAttributeLength is <= 0 or > 4000 ? "MAX" : maxAttributeLength.ToString())})";
                }
            }

            return " " + sqlType + ((applyNotNull && attr.Base.Mandatory) ? " NOT NULL" : "");
        }

        private static string GetSqlType(this Dimension dim, bool withType, bool allowNull)
        {
            if (!withType)
            {
                return null;
            }

            var sqlType = "int";

            if (dim.Base.TimeDimension)
            {
                sqlType = "varchar(30)";
            }
            else if (!dim.Base.HasCodedRepresentation())
            {
                if (dim.Base.Representation?.TextFormat == null)
                {
                    sqlType = "[nvarchar](4000)";
                }
                else
                {
                    var size = dim.Base.Representation.TextFormat.MaxLength is > 0 and <= 4000
                        ? dim.Base.Representation.TextFormat.MaxLength.ToString()
                        : "4000";

                    sqlType = $"[nvarchar]({size})";
                }
            }

            return " " + sqlType + (allowNull ? "" : " NOT NULL");
        }

        public static SqlDbType GetSqlType(this MetadataAttribute attr)
        {
            return SqlDbType.NVarChar;
        }

        private static string GetSqlType(this MetadataAttribute attr, bool withType)
        {
            if (!withType)
            {
                return null;
            }

            return "[nvarchar](MAX) ";
        }

        public static string GetSqlValue(this Attribute attr, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "NULL";
            }

            return attr.Base.HasCodedRepresentation() ? value : $"{value.Replace("'", "''")}";
        }

        public static object GetSqlMinValue(this Attribute attr, bool asSqlString)
        {
            return attr.Base.HasCodedRepresentation() ? (asSqlString? "-1":-1) : (asSqlString ? "'#N/A'" : "#N/A");
        }

        public static string GetSqlValue(this MetadataAttribute attr, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "NULL";
            }

            return $"{value.Replace("'", "''")}";
        }
        
        public static object GetObjectFromString(this Attribute attribute, string attrStrVal, ICodeTranslator codeTranslator, StagingRowActionEnum action, bool deleteEmptyAttribute = false)
        {
            if (string.IsNullOrWhiteSpace(attrStrVal) && !(deleteEmptyAttribute && action == StagingRowActionEnum.Delete))
                return null;

            //Indicate that the value of this component is present in the delete operation
            if (action == StagingRowActionEnum.Delete)
            {
                if (attribute.Base.HasCodedRepresentation())
                    return ColumnPresentDbValue;
                
                return ColumnPresentDbValue.ToString();
            }

            if (attribute.Base.HasCodedRepresentation())
            {
                return codeTranslator.TranslateCodeToId(attribute.Code, attrStrVal);
            }

            if (attribute.Code.Equals(EmbargoTime, StringComparison.InvariantCultureIgnoreCase) && DateTime.TryParse(attrStrVal, out var attrDateTimeVal))
            {
                return attrDateTimeVal.ToUniversalTime();
            }

            return attrStrVal;
        }

        public static bool HasSupportDateTimeAnnotation(this IDataStructureObject dsd)
        {
            var hasAnnotation = dsd.Annotations.Any(a =>
                a.Type.Equals("SUPPORT_DATETIME", StringComparison.InvariantCultureIgnoreCase));

            return hasAnnotation;
        }

        private static string GetMaxLengthOfTextAttributeAnnotationValue(this IDataStructureObject dsd, string annotationType)
        {
            var annotation = dsd?.Annotations.FirstOrDefault(a => 
                a.Type.Equals(annotationType, StringComparison.InvariantCultureIgnoreCase) 
                    &&
                !string.IsNullOrWhiteSpace(a.Title)
            );

            return annotation?.Title;
        }

        private static int GetMaxLengthFromAnnotationOrConfig(this Dsd dsd, int? maxLength, string annotationType)
        {
            var annotation = dsd.Base.GetMaxLengthOfTextAttributeAnnotationValue(annotationType);

            if (!string.IsNullOrEmpty(annotation))
            {
                if (int.TryParse(annotation, out int maxLengthOfAttribute))
                {
                    return maxLengthOfAttribute;
                }

                Log.Warn(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .MaximumTextAttributeLengthInvalidDSDAnnotationValue), dsd.FullId, annotation));
            }

            return maxLength ?? 150;
        }

        public static int GetMaxLengthOfTextAttributeFromConfig(this Dsd dsd, int? maxLength)
            => GetMaxLengthFromAnnotationOrConfig(dsd, maxLength, MaxTextAttributeLengthAnnotation);

        public static string GetSqlType(this PrimaryMeasure primaryMeasure, bool supportsIntentionallyMissingValues)
        {
            if (primaryMeasure.Codelist != null)
            {
                // Primary measure is coded, the code value is stored
                return "[varchar](150)"; // Identical to the data type of ID column in code list tables
            }

            // Measure is not coded, text format of representation defines the sql data type
            if (primaryMeasure.TextFormat == null)
            {
                // No representation is defined in DSD so the default string textFormat is applied.
                return "[nvarchar](max)";
            }

            switch (primaryMeasure.TextFormat.TextType.EnumType)
            {
                case TextEnumType.String:
                case TextEnumType.Alpha:
                case TextEnumType.Alphanumeric:
                case TextEnumType.Uri:
                {
                    var size = primaryMeasure.TextFormat.MaxLength is > 0 and <= 4000
                        ? primaryMeasure.TextFormat.MaxLength.ToString()
                        : "MAX";

                    return $"[nvarchar]({size})";
                    }
                case TextEnumType.Numeric:
                case TextEnumType.BigInteger:
                case TextEnumType.Decimal:
                {
                    var size = primaryMeasure.TextFormat.MaxLength is > 0 and <= 8000
                        ? primaryMeasure.TextFormat.MaxLength.ToString()
                        : "MAX";

                    return $"[varchar]({size})";
                }
                case TextEnumType.Integer:
                case TextEnumType.Count:
                    return "[int]";
                case TextEnumType.Long:
                    return "[bigint]";
                case TextEnumType.Short:
                case TextEnumType.Boolean:
                    return "[smallint]";
                case TextEnumType.Float:
                    return supportsIntentionallyMissingValues ? "[varchar](13)" : "[float](24)";
                case TextEnumType.Double:
                    return supportsIntentionallyMissingValues ? "[varchar](22)" : "[float](53)";
                case TextEnumType.ObservationalTimePeriod:
                case TextEnumType.StandardTimePeriod:
                case TextEnumType.BasicTimePeriod:
                case TextEnumType.GregorianTimePeriod:
                case TextEnumType.GregorianYear:
                case TextEnumType.GregorianYearMonth:
                case TextEnumType.GregorianDay:
                case TextEnumType.ReportingTimePeriod:
                case TextEnumType.ReportingYear:
                case TextEnumType.ReportingSemester:
                case TextEnumType.ReportingTrimester:
                case TextEnumType.ReportingQuarter:
                case TextEnumType.ReportingMonth:
                case TextEnumType.ReportingWeek:
                case TextEnumType.ReportingDay:
                case TextEnumType.DateTime:
                case TextEnumType.TimesRange:
                case TextEnumType.Month:
                case TextEnumType.MonthDay:
                case TextEnumType.Day:
                case TextEnumType.Time:
                case TextEnumType.Duration:
                    {
                        return "[varchar](100)";
                    }
                default:
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.InvalidValueFormatNotSupported),
                        primaryMeasure.TextFormat.TextType.EnumType,
                        primaryMeasure.FullId));
            }
        }

        public static object GetPrimaryMeasureObjectFromString(this Dsd dsd, string observationValueStr, StagingRowActionEnum action, CultureInfo cultureInfo)
        {
            if (string.IsNullOrWhiteSpace(observationValueStr))
            { 
                return null;
            }

            //Indicate that the value of this component should be set to NULL
            if (action == StagingRowActionEnum.Delete)
            {
                observationValueStr = ColumnPresentDbValue.ToString();
            }


            if (dsd.PrimaryMeasure.Codelist != null || dsd.PrimaryMeasure.TextFormat == null)
            {
                // Primary measure is coded or
                // Measure is not coded and no representation is defined in DSD so the default string textformat is applied or
                return observationValueStr;
            }

            switch (dsd.PrimaryMeasure.TextFormat.TextType.EnumType)
            {
                case TextEnumType.Integer:
                case TextEnumType.Count:
                    return int.TryParse(observationValueStr, out var integerValue) ? integerValue : (object) observationValueStr;
                case TextEnumType.Long:
                    return long.TryParse(observationValueStr, out var longValue) ? longValue : (object)observationValueStr;
                case TextEnumType.Short:
                    return short.TryParse(observationValueStr, out var shortValue) ? shortValue : (object)observationValueStr;
                case TextEnumType.Float:
                    if (dsd.SupportsIntentionallyMissingValues)
                    {
                        return observationValueStr.Equals(IntentionallyMissingValue.Numerical.Value, StringComparison.OrdinalIgnoreCase) ? IntentionallyMissingValue.Numerical.Value : (object)observationValueStr;
                    }

                    return float.TryParse(observationValueStr, NumberStyles.Float, cultureInfo.NumberFormat, out var floatValue) ? floatValue : (object)observationValueStr;
                case TextEnumType.Double:
                    if (dsd.SupportsIntentionallyMissingValues)
                    {
                        return observationValueStr.Equals(IntentionallyMissingValue.Numerical.Value, StringComparison.OrdinalIgnoreCase) ? IntentionallyMissingValue.Numerical.Value : (object)observationValueStr;
                    }

                    return double.TryParse(observationValueStr, NumberStyles.Float, cultureInfo.NumberFormat, out var doubleValue) ? doubleValue : (object)observationValueStr;
                case TextEnumType.Boolean:
                {
                    if (observationValueStr.Equals("1"))
                    {
                        return 1;
                    }

                    if (observationValueStr.Equals("0"))
                    {
                        return 0;
                    }

                    return bool.TryParse(observationValueStr, out var boolValue) ? (short)(boolValue ? 1 : 0) : (object)observationValueStr;
                }
                default:
                {
                    return observationValueStr;
                }
            }
        }

        public static string HarmonizeSpecialCaseValues(this string strVal)
        {
            return string.IsNullOrEmpty(strVal) || strVal.Equals("*") ? null : strVal;
        }

        #region Datareader

        public static T ColumnValue<T>(this IDataReader rd, string column, T def = default(T))
        {
            return rd[column].ColumnValue(def);
        }

        public static T ColumnValue<T>(this IDataReader rd, int columnIndex, T def = default(T))
        {
            return rd[columnIndex].ColumnValue(def);
        }

        public static T ColumnValue<T>(this DataRow row, string column, T def = default(T))
        {
            return row[column].ColumnValue(def);
        }

        public static T ColumnValue<T>(this DataRow row, int columnIndex, T def = default(T))
        {
            return row[columnIndex].ColumnValue(def);
        }

        private static T ColumnValue<T>(this object o, T def = default(T))
        {
            if (o == DBNull.Value || o == null)
                return def;

            if (o is T)
                return (T)o;

            return (T)Convert.ChangeType(o, typeof(T));
        }

        #endregion

        #region IObservation

        public static bool IsDeleteOperation(this IObservation observation, Dataflow dataflow)
        {
            var observationAttributes = dataflow.Dsd.Base.ObservationAttributes.Select(a => a.Id).ToList();

            return string.IsNullOrEmpty(observation.ObservationValue) && observation.Attributes.All(a =>
                       string.IsNullOrEmpty(a.Code) ||
                       !observationAttributes.Contains(a.Concept, StringComparer.InvariantCultureIgnoreCase));
        }

        public static string GetKey(this IObservation observation, bool withTime = true)
        {
            return string.Join(":", observation.SeriesKey.Key.Select(x => x.Code)) + (withTime ? ":" + observation.ObsTime : string.Empty);
        }

        public static string GetKeyStartEndPeriod(this IObservation observation, bool hasReportingYearStartDayAttr)
        {
            if (string.IsNullOrWhiteSpace(observation.ObsTime))
                return string.Join(":", observation.SeriesKey.Key.Select(x => x.Code));

            try
            {
                var (periodStart, periodEnd) = DateUtils.GetPeriod(
                    observation.ObsTime,
                    hasReportingYearStartDayAttr ? observation.GetAttribute(AttributeObject.Repyearstart)?.Code : null
                );

                var timeKeys = ":" + periodStart + ":" + periodEnd;
                return string.Join(":", observation.SeriesKey.Key.Select(x => x.Code)) + timeKeys;
            }
            catch
            {
                //Exclude time when it is wildCarded
                return string.Join(":", observation.SeriesKey.Key.Select(x => x.Code));
            }
        }
        
        public static string GetFullKey(this IObservation observation, bool withTime = true, string dimTimeConcept = "")
        {
            return string.Join(",", observation.SeriesKey.Key.Where(k => !string.IsNullOrEmpty(k.Code))
                .Select(x => $"{x.Concept}:{x.Code}")) + (withTime ? "," + $"{dimTimeConcept}:{observation.ObsTime}" : string.Empty);
        }
        
        #endregion

        #region IAttributeObject

        public static IList<string> GetDimensionReferences(this IAttributeObject attribute) {

            if (attribute.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
            {
                return attribute.DimensionReferences;
            }

            if (attribute.AttachmentLevel == AttributeAttachmentLevel.DataSet)
            {
                return new List<string>();
            }

            var dsd = (IDataStructureObject) attribute.IdentifiableParent.IdentifiableParent;

            if (dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingDsdForAttribute),
                    attribute.GetFullIdPath(true))
                ); 
            }

            if (attribute.AttachmentLevel == AttributeAttachmentLevel.Observation)
            {
                return dsd.DimensionList.Dimensions.Select(d => d.Id).ToList();
            }

            if (attribute.AttachmentLevel != AttributeAttachmentLevel.Group)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnknownAttributeAttachmentLevel),
                    attribute.GetFullIdPath(true),
                    attribute.AttachmentLevel,
                    $"{dsd.AgencyId}:{dsd.Id}({dsd.Version})")
                );
            }

            IGroup group = null;
            foreach(var g in dsd.Groups)
            {
                if(string.Equals(g.Id, attribute.AttachmentGroup, StringComparison.OrdinalIgnoreCase))
                {
                    group = g;
                    break;
                }
            }

            if (group == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingGroupForAttribute),
                    attribute.AttachmentGroup,
                    attribute.GetFullIdPath(true))
                );
            }

            return group.DimensionRefs;
        }

        #endregion

        #region IKeyable

        public static string GetKey(this IKeyable keyable)
        {
            return string.Join(":", keyable.Key.Select(x => x.Code));
        }

        public static string GetFullKey(this IKeyable keyable, bool includeConcept = false)
        {
            var dimensionReferences = keyable.DataStructure.GetDimensions();
            var ret = includeConcept ?
                string.Join(",", dimensionReferences.Where(d => !string.IsNullOrEmpty(keyable.GetKeyValue(d.Id)))
                    .Select(d => $"{d.Id}:{keyable.GetKeyValue(d.Id)}"))
                : string.Join(":", dimensionReferences.Select(d => keyable.GetKeyValue(d.Id)));
            return ret;
        }

        public static IList<IKeyValue> GetKeyOfAttribute(this IKeyable keyable, IAttributeObject attribute)
        {
            var ret = new List<IKeyValue>();

            var dimensionReferences = attribute.GetDimensionReferences();

            foreach (var dimRef in dimensionReferences)
            {
                foreach(var key in keyable.Key)
                {
                    if (string.Equals(dimRef, key.Concept, StringComparison.OrdinalIgnoreCase))
                    {
                        ret.Add(new KeyValueImpl(key?.Code ?? "", dimRef));
                        break;
                    }
                }
            }

            return ret;
        }

        #endregion
    }

}
