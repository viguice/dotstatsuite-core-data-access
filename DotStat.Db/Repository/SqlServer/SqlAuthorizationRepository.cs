﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Model;
using DotStat.DB.Dapper;

namespace DotStat.Db.Repository.SqlServer
{
    public class SqlAuthorizationRepository : DapperBaseRepository<UserAuthorization, int>, IAuthorizationRepository
    {
        private IGeneralConfiguration _config;
        protected override string ConnectionString => _config.DotStatSuiteCoreCommonDbConnectionString;

        public SqlAuthorizationRepository(IGeneralConfiguration config)
        {
            _config = config;
        }

        public IEnumerable<UserAuthorization> GetAll()
        {
            return base.GetList();
        }

        public IEnumerable<UserAuthorization> GetByUser(DotStatPrincipal principal)
        {
            var query = new StringBuilder("where [UserMask]='*' or (IsGroup=0 and [UserMask]=@UserId)");

            if (principal.Groups.Any())
            {
                query.Append(" or (IsGroup=1 and [UserMask] in @Groups)");
            }

            return base.GetList(query.ToString(), 
                new
                {
                    principal.UserId,
                    principal.Groups
                }
            );
        }
    }
}