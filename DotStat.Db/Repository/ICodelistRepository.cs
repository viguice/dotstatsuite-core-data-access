﻿using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Db.Repository
{
    public interface ICodelistRepository
    {
        Task<string[]> GetDimensionCodesFromDb(int codeListId, CancellationToken cancellationToken);

    }
}