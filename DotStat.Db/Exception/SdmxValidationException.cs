﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;
using DotStat.Db.Validation;

namespace DotStat.DB.Exception
{
    [ExcludeFromCodeCoverage]
    public abstract class SdmxValidationException : DotStatException
    {
        public IValidationError Error { get;}

        protected SdmxValidationException(IValidationError error)
        {
            Error = error;
        }

        public override string Message => Error.ToString();
    }
}