﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Validation
{
    public interface IKeyableDatabaseValidator
    {
        Task<bool> Validate(IDotStatDb dotStatDataDb, ICodeTranslator translator, Dataflow dataflow, List<Attribute> reportedAttributes, DbTableVersion tableVersion, int? maxErrorCount, CancellationToken cancellationToken);

        List<IValidationError> GetErrors();

        string GetErrorsMessage();
    }
}
