﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class ReportingMonthValidator : PatternValidator
    {
        private const string ValidationExpression =
            @"^\d{4}\-M(0[1-9]|1[0-2])(Z|(\+|\-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotReportingMonth;

        public ReportingMonthValidator(bool allowNullValue = true) : base (ValidationExpression, allowNullValue)
        {
        }
    }
}
