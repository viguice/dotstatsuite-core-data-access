﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class TimeRangeValidator : PatternValidator
    {
        private const string ValidationExpression =
            @"^-?([1-9][0-9]{3,}|0[0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])
                  (T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\.[0-9]+)?|(24:00:00(\.0+)?)))?
				  (Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?
                  /
                -?P( ( ( [0-9]+Y([0-9]+M)?([0-9]+D)?
                       | ([0-9]+M)([0-9]+D)?
                       | ([0-9]+D)
                       )
                       (T ( ([0-9]+H)([0-9]+M)?([0-9]+(\.[0-9]+)?S)?
                          | ([0-9]+M)([0-9]+(\.[0-9]+)?S)?
                          | ([0-9]+(\.[0-9]+)?S)
                          )
                       )?
                    )
                  | (T ( ([0-9]+H)([0-9]+M)?([0-9]+(\.[0-9]+)?S)?
                       | ([0-9]+M)([0-9]+(\.[0-9]+)?S)?
                       | ([0-9]+(\.[0-9]+)?S)
                       )
                    )
                  )$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotTimeRange;

        public TimeRangeValidator(bool allowNullValue = true) : base (ValidationExpression, allowNullValue)
        {
        }
    }
}
