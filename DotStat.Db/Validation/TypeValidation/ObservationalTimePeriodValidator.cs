﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class ObservationalTimePeriodValidator : ComplexSdmxTypeValidator
    {
        public ObservationalTimePeriodValidator(bool allowNullValue = true) : base(
            new ISdmxTypeValidator[]
            {
                new StandardTimePeriodValidator(allowNullValue), 
                new TimeRangeValidator(allowNullValue), 
            }, ComplexValidationMode.Any, allowNullValue)
        {
        }

        public override bool IsValid(string observationValue)
        {
            if (base.IsValid(observationValue))
            {
                return true;
            }

            ValidationError = ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod;

            return false;
        }
    }
}
