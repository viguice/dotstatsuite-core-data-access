SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- SQL Server Agent (and it's 'msdb' database) is not supported on Azure SQL Database.
-- As a workaround Azure Elastic Job Agent (https://docs.microsoft.com/en-us/azure/azure-sql/database/job-automation-overview#elastic-job-agent) 
-- or Azure Logic App (https://www.mssqltips.com/sqlservertip/6420/create-azure-sql-database-scheduled-jobs/) may be created 
-- and configured to execute the dbo.IndexOptimize stored procedure on a scheduled manner. (Not done by the script below.)
IF SERVERPROPERTY('EngineEdition') <> 5 
BEGIN
    DECLARE @CurrentDatabase AS nvarchar(max),
            @job_name AS nvarchar(max),
            @commandValue AS nvarchar(max),
            @Job AS nvarchar(max);

    SELECT @CurrentDatabase = DB_NAME();

    SELECT @job_name = 'Daily '+@CurrentDatabase+' Db Index Optimization';

    --See more https://ola.hallengren.com/sql-server-index-and-statistics-maintenance.html
    SELECT @commandValue = N'''EXEC ' + @CurrentDatabase + '.dbo.IndexOptimize 
        @Databases = ''''' + @CurrentDatabase + ''''', 
        @FragmentationLow = NULL,
        @FragmentationMedium = ''''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'''',
        @FragmentationHigh = ''''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'''',
        @FragmentationLevel1 = 5,
        @FragmentationLevel2 = 30,
        @UpdateStatistics = ''''ALL'''',
        @OnlyModifiedStatistics = ''''N''''';

    -- Azure SQL Database: Checking if job exists added to dynamic scripts as msdb is not available 
    SET @Job = 
    N'USE msdb ; 
      IF NOT EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = ''' + @job_name + ''')
      BEGIN

        EXEC dbo.sp_add_job  
            @job_name = ''' + @job_name + '''  

        EXEC sp_add_jobstep  
            @job_name =  ''' + @job_name + ''',  
            @step_name = ''Index Optimization'',  
            @subsystem = ''TSQL'',   
            @command = N' + @commandValue  + ''', 
            @database_name = ''' + @CurrentDatabase + ''',
            @retry_attempts = 5,  
            @retry_interval = 5; 

        EXEC dbo.sp_add_schedule 
            @schedule_name = N''NightRun_' + @CurrentDatabase + ''',  
            @enabled  = 1 ,
            @freq_type = 4, 
            @freq_interval = 1,
            @active_start_time = 010000 ; --1:00 am 

        EXEC sp_attach_schedule  
           @job_name =''' + @job_name + ''',  
            @schedule_name = N''NightRun_' + @CurrentDatabase + '''

        EXEC dbo.sp_add_jobserver  
            @job_name =''' + @job_name + ''';
            
    END';

    EXECUTE sp_executesql @Job
END