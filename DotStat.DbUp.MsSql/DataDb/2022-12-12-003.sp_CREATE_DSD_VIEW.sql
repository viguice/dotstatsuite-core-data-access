CREATE OR ALTER PROC CREATE_DSD_VIEW
	@DSD_ID int,
	@table_version char(1)
AS
BEGIN
	DECLARE
		@sdmxId as NVARCHAR(1000),
		@U_DSD_VERSION_ID varchar(20),
		@NewLineChar AS CHAR(2) =  CHAR(10),
		@isLiveSet as bit,
		@isPITset as bit,			
		@HasTimeDim as bit = 0,
		@TimeDimId as int = 0,
		@msg NVARCHAR(MAX),
		@sql NVARCHAR(MAX),
		@SelectPart NVARCHAR(MAX),
		@FromPart NVARCHAR(MAX),
		@WherePart NVARCHAR(MAX)

	DECLARE
		@DSD_Components TABLE(
			COMP_ID int not null,
			ID varchar(50) not null, 
			[TYPE] varchar(50) not null, 	
			CL_ID int, 
			ATT_ASS_LEVEL varchar(50),
			ATT_STATUS varchar(11),
			IS_TIME_DIM_REFERENCED bit not null
		)

    SELECT
		@U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version,
		@sdmxId = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')',
		@isLiveSet = CASE WHEN [DSD_LIVE_VERSION] IS NULL THEN 0 ELSE 1 END,
		@isPITset = CASE WHEN [DSD_PIT_VERSION] IS NULL THEN 0 ELSE 1 END	
	FROM [management].[ARTEFACT] 		
	WHERE [type] = 'DSD' AND ART_ID = @DSD_ID;

	--- check if fact table exists
	IF (NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'data' AND  TABLE_NAME = 'FACT_' + @U_DSD_VERSION_ID))
	BEGIN
		-- SKIP when no fact table has been created and no [DSD_LIVE_VERSION], nor [DSD_PIT_VERSION] has been set
		IF( @isLiveSet = 0 AND @isPITset = 0)
		BEGIN
			RETURN	
		END

		set @msg = 'The following error was found while trying to recreate the DSD Views for the DSD ['+ @sdmxId +'] (The creation of the view will be skiped): The table [data].[FACT_'+ @U_DSD_VERSION_ID + '] does not exist in the database, but a value has been set for the fields [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] in the [management].[ARTEFACT] for the given DSD.';

		throw 51000, @msg, 1;
	END

	-- retrieve IF DSD has TIME dimension
	SELECT @HasTimeDim=1, @TimeDimId = COMP_ID 
	FROM [management].[COMPONENT] WHERE [TYPE] ='TimeDimension' AND DSD_ID = @DSD_ID 

	-- get components
	INSERT INTO @DSD_Components
	SELECT c.COMP_ID,c.ID,c.[TYPE],c.[CL_ID],c.[ATT_ASS_LEVEL], c.[ATT_STATUS], CASE WHEN RT.ATTR_ID IS NULL THEN 0 ELSE 1 END
	FROM [management].[COMPONENT] c 	
		LEFT JOIN (SELECT [ATTR_ID] FROM [management].[ATTR_DIM_SET] WHERE DIM_ID = @TimeDimId) AS RT ON C.COMP_ID = RT.ATTR_ID
	WHERE c.DSD_ID=@DSD_ID and [Type] in ('Dimension','Attribute') AND ISNULL(c.[ATT_ASS_LEVEL], '' )<>'DataSet'

	----------------------------START BUILD SELECT STATEMENT---------------------------

	SET @SelectPart= 'SELECT [FI].[SID],[FA].[VALUE] AS [OBS_VALUE]' 
	+	CASE @HasTimeDim
			WHEN 1 THEN ',[FA].[PERIOD_SDMX] AS [TIME_PERIOD],[FA].[PERIOD_START],[FA].[PERIOD_END]' 
			ELSE ''
		END
	
	SET @SelectPart += ISNULL(STUFF(( 
		SELECT ','+
			CASE 
			--coded dimensions and coded attributes
			WHEN C.[CL_ID] IS NOT NULL 
				THEN '[CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+']'+@NewLineChar
			--Non-coded dimensions
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Dimension'
				THEN '[FI].[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
			--Non-coded attributes at observation level
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'Observation'
				THEN '[FA].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
			--Non-coded attributes at referencing time dimension
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Attribute' AND C.[IS_TIME_DIM_REFERENCED] = 1
				THEN '[FA].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
			--Non-coded attributes at dimgroup level
			WHEN C.[TYPE] = 'Attribute' AND (C.[ATT_ASS_LEVEL] IN ('DimensionGroup','Group'))
				THEN '[ATTR].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
			END
		FROM @DSD_Components C  FOR XML PATH('')
	), 1, 0, ''), '')

	SET @SelectPart+= ',[FA].[LAST_UPDATED]'
	--------------------------------START BUILD WHERE STATEMENT-------------------------------

	SELECT @WherePart ='WHERE EXISTS (SELECT TOP 1 1 FROM [data].[FACT_' + @U_DSD_VERSION_ID+ '] AS FA WHERE FI.[SID] = FA.[SID])';

	--------------------------------START BUILD FROM STATEMENT-------------------------------
	SELECT @FromPart='FROM [data].[FILT_'+CAST(@DSD_ID AS VARCHAR)+'] FI' + @NewLineChar 
	+ 'LEFT JOIN [data].[FACT_'+ @U_DSD_VERSION_ID + '] FA ON FI.[SID] = FA.[SID]' + @NewLineChar

	IF exists (SELECT top 1 1 FROM @DSD_Components WHERE [ATT_ASS_LEVEL] IN ('DimensionGroup','Group'))
	BEGIN
		SELECT @FromPart += 'LEFT JOIN [data].[ATTR_'+ @U_DSD_VERSION_ID + '] AS [ATTR] ON FI.[SID] = ATTR.[SID]'+@NewLineChar 
		SELECT @WherePart += ' OR EXISTS (SELECT TOP 1 1 FROM [data].[ATTR_' + @U_DSD_VERSION_ID + '] AS [ATTR] WHERE FI.[SID] = ATTR.[SID])';	
	END

	--------------------------------START JOIN  STATEMENT------------------------------------
	SELECT @FromPart += ISNULL(STUFF(( 
		SELECT 
			CASE 
			--coded dimensions
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension'
				THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON FI.[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
			--coded attributes at observation level
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'Observation' 
				THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON FA.[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
			--coded attributes at group level
			WHEN C.[CL_ID] IS NOT NULL AND C.[ATT_ASS_LEVEL] IN ('DimensionGroup','Group')
				THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON ATTR.[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
			END
		FROM @DSD_Components C FOR XML PATH('')
	), 1, 0, ''),'') 

	print 'Recreating DSD VIEW(' + @table_version + ') for DF: ' + @sdmxId + ', ID:' + CAST(@DSD_ID AS VARCHAR);

	SET @sql = 'CREATE OR ALTER VIEW [data].[VI_CurrentDataDsd_' + @U_DSD_VERSION_ID + '] AS'  + @NewLineChar 
		+ @SelectPart + @NewLineChar 
		+ @FromPart + @NewLineChar
		+ @WherePart

	exec sp_executesql @sql;
END