
IF EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[LOGS]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    ALTER TABLE [management].[LOGS]
	ADD  
        [THREAD] [nvarchar](255) NULL,
        [APPLICATION] [nvarchar](255) NULL,
        [URLREFERRER] [nvarchar](max) NULL,
        [HTTPMETHOD] [nvarchar](max) NULL,
        [REQUESTPATH] [nvarchar](max) NULL,
        [QUERYSTRING] [nvarchar](max) NULL

END