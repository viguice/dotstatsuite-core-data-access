SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
 
DECLARE 	
	@DSD_ID int,
	@SDMX_ID nvarchar(1000),

	@sql_migration NVARCHAR(MAX),	
	@msg nvarchar(1000),

	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10),
	@U_DSD_VERSION_ID varchar(20),
	@Transaction_ID int,
	@HasTimeDim bit;

DECLARE
	@DSD_Dimensions TABLE(
		COMP_ID int not null,
		ID varchar(50) not null, 
		CL_ID int
	);

	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY, SDMX_ID nvarchar(1000));

DECLARE 
	@VERSIONS TABLE ([version] char(1));

INSERT into @DSD_IDS 
	SELECT ART_ID, [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')' 
	FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID
	
--------------------------------------------------------
--Create new transaction for DSD
SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
INSERT 
	INTO [management].[DSD_TRANSACTION]([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
	VALUES (@transaction_ID, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)
--------------------------------------------------------
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

    SELECT TOP 1 @DSD_ID = ART_ID, @SDMX_ID=SDMX_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
	print '';
	print 'Migrating DSD with Internal ID: '+ CAST(@DSD_ID AS VARCHAR);

	-- Retrieve IF DSD has TIME dimension
	SELECT @HasTimeDim=1 FROM [management].[COMPONENT] WHERE [TYPE] ='TimeDimension' AND DSD_ID = @DSD_ID 

	-- Get DSD dimensions
	DELETE @DSD_Dimensions;

	INSERT INTO @DSD_Dimensions
	SELECT c.COMP_ID,c.ID,c.[CL_ID]
	FROM [management].[COMPONENT] c
	WHERE c.DSD_ID = @DSD_ID and [Type] = 'Dimension'
	
--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	BEGIN TRY
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
			-- select next version
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version

			SET @U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;

			BEGIN TRAN
			
					-- Add LAST_UPDATED column for ATTR_{DSD_ID}_{A/B} Tables
				IF EXISTS(SELECT 1 FROM sys.tables WHERE Object_ID = Object_ID(N'data.ATTR_' + @U_DSD_VERSION_ID))
					AND NOT EXISTS(SELECT 1 FROM sys.columns WHERE [Name] = N'LAST_UPDATED' AND Object_ID = Object_ID(N'data.ATTR_' + @U_DSD_VERSION_ID))
				BEGIN		
					set @sql_migration = 'ALTER table data.ATTR_'+ @U_DSD_VERSION_ID +' ADD
		LAST_UPDATED [datetime] NOT NULL DEFAULT (GETDATE())';	

					exec sp_executesql @sql_migration;
				END
				
				-- Add LAST_UPDATED column for ATTR_{DSD_ID}_{A/B}_DF Tables	
				IF EXISTS(SELECT 1 FROM sys.tables WHERE Object_ID = Object_ID(N'data.ATTR_' + @U_DSD_VERSION_ID + '_DF'))
					AND NOT EXISTS(SELECT 1 FROM sys.columns WHERE [Name] = N'LAST_UPDATED' AND Object_ID = Object_ID(N'data.ATTR_' + @U_DSD_VERSION_ID + '_DF'))
				BEGIN	
					set @sql_migration = 'ALTER table data.ATTR_'+ @U_DSD_VERSION_ID+ '_DF ADD
		LAST_UPDATED [datetime] NOT NULL DEFAULT (GETDATE())';	

					exec sp_executesql @sql_migration;
				END
				--CREATE/RECREATE views

				-- Create df views replace 
				exec dbo.CREATE_DF_VIEWS_REPLACE @DSD_ID, @table_version; 
							
				-- recreate df views
				exec dbo.CREATE_DF_VIEWS @DSD_ID, @table_version; 

			COMMIT TRAN
		END		
	END TRY  
	BEGIN CATCH 
		IF (XACT_STATE()) <> 0 ROLLBACK TRAN
						
		SELECT @msg= 'The following error was found while trying to migrate DSD ['+ @SDMX_ID +'] (' + @U_DSD_VERSION_ID + '):'
			+ @NewLineChar + 'ErrorLine:' + CAST(ERROR_LINE() AS VARCHAR)
			+ @NewLineChar + 'ErrorNumber:' + CAST(ERROR_NUMBER() AS VARCHAR)
			+ @NewLineChar + 'ErrorMessage:' + ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2023-03-13-02.AddUpdatedAfterHighLvlAttrTables.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
				
	END CATCH

END

--update transaction info
UPDATE  [management].[DSD_TRANSACTION] SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 1
WHERE [TRANSACTION_ID] = @Transaction_ID