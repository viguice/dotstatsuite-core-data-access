--Update previous AdminRole value (2047) to new AdminRole value (4095) as value PermissionType.AdminRole changed after addition of PermissionType.CanReadPitData (2048)
UPDATE [dbo].[AUTHORIZATIONRULES]
    SET PERMISSION = 4095
WHERE PERMISSION = 2047

GO