﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Db.DB;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.MappingStore;

namespace DotStat.Test.Moq
{
    public class TestManagementRepository
    {
        private Dataflow _df;
        private Dictionary<int, Codelist> _codelists;

        public IDotStatDb DotStatDb => throw new NotImplementedException();

        public IArtefactRepository ArtefactRepository => throw new NotImplementedException();

        public IAttributeRepository AttributeRepository => throw new NotImplementedException();

        public IDataStoreRepository DataStoreRepository => throw new NotImplementedException();

        public IMetadataComponentRepository MetaMetadataComponentRepository => throw new NotImplementedException();

        public IMetadataStoreRepository MetadataStoreRepository => throw new NotImplementedException();

        public IObservationRepository ObservationRepository => throw new NotImplementedException();

        public ITransactionRepository TransactionRepository => throw new NotImplementedException();

        public IComponentRepository ComponentRepository => throw new NotImplementedException();

        public ICodelistRepository CodelistRepository => throw new NotImplementedException();

        public IMappingStoreDataAccess MappingStoreDataAccess => throw new NotImplementedException();

        public TestManagementRepository(Domain.Dataflow dataflow)
        {
            this.FillIdsFromDisseminationDb(dataflow);
        }
        

        public string[] GetDimensionCodesFromDb(int codeListId)
        {
            Codelist cl;

            if(!_codelists.TryGetValue(codeListId, out cl))
                throw new InvalidOperationException($"Codelist with [{codeListId}] not found");

            var list = new string[cl.Codes.Count + 1];
            Array.Copy(cl.Codes.Select(c=>c.Code).ToArray(), 0, list, 1, list.Length - 1);
            return list;
        }

        public void FillIdsFromDisseminationDb(Dataflow dataflow)
        {
            _df                 = dataflow;
            _codelists          = new Dictionary<int, Codelist>();
            // --------------------------------------------

            _df.Dsd.LiveVersion = 'A';
            _df.Dsd.DbId        = 7;
            _df.DbId = 1;

            for (var i = 0; i < dataflow.Dsd.Dimensions.Count; ++i)
            {
                var dimension   = dataflow.Dsd.Dimensions[i];
                var id          = i + 101;
                dimension.DbId  = id;

                if (!dimension.Base.TimeDimension)
                {
                    dimension.Codelist.DbId = id;
                    _codelists[id] = dimension.Codelist;
                }
            }

            for (var i = 0; i < dataflow.Dsd.Attributes.Count; ++i)
            {
                var attr = dataflow.Dsd.Attributes[i];
                var id = i + 201;
                attr.DbId = id;

                if (attr.Base.HasCodedRepresentation())
                {
                    attr.Codelist.DbId = id;
                    _codelists[id] = attr.Codelist;
                }
            }
        }
        
        private T[] GetCodelist<T>(string fileName, Func<int, string[], T> readCode)
        {
            T[] codelist = null;

            using (var sr = new StreamReader(fileName))
            {
                var line = sr.ReadLine();

                if (string.IsNullOrEmpty(line))
                    throw new InvalidOperationException("Codelist is empty");

                var arr         = line.Split(',');
                var id          = int.Parse(arr[0]);
                codelist        = new T[id + 1]; // dimension desc order, first row has max capasity
                codelist[id]    = readCode(id, arr);

                while ((line = sr.ReadLine()) != null)
                {
                    arr = line.Split(',');
                    id = int.Parse(arr[0]);
                    codelist[id] = readCode(id, arr);
                }
            }

            return codelist;
        }

        public Task CheckManagementTables(int transactionId, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<bool> TryNewTransaction(Transaction transaction, TargetVersion transferTargetVersion, Dataflow dataflow, bool isRollbackOrRestore, DotStatPrincipal principal, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<bool> TryNewTransactionWithNoDsd(Transaction transaction, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<bool> TryNewTransactionForCleanup(Transaction transaction, string agency, string id, SdmxVersion version, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CloseTransaction(Transaction transaction, Dataflow dataflow, bool PITRestorationAllowed, bool wasDataModified, ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task CleanUpFailedTransaction(Transaction transaction, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Rollback(Dataflow dataflow, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Restore(Dataflow dataflow, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task ApplyPITRelease(Dataflow dataflow, bool isRestorationAllowed, ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task ManageMappingSets(Dsd dsd, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<bool> InitializeAllMappingSets(Transaction transaction, DotStatPrincipal principal, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CleanUpDsd(int dsdDbId, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CleanUpMsdOfDsd(int dsdDbId, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
